﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class Manager : MonoBehaviour
{
    public GameObject inventory, ItemStats;
    public CanvasManager _canvasManager;
    RaycastHit2D hit;
    Ray ray;

    private void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        onHover(ray, hit);

    }
    public void onHover(Ray ray, RaycastHit2D hit)

    {
      
        hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        if (hit.collider)
        {
            switch (hit.collider.gameObject.layer)
            {

                case 12:
                    if (hit.collider.gameObject.GetComponent<Item>() != null)
                    {

                        ItemStats.GetComponent<ItemStatsUI>().selectedItem = hit.collider.gameObject;
                        ItemStats.SetActive(false);
                        ItemStats.SetActive(true);
                        

                    }
                    else
                    {
                        ItemStats.SetActive(false);
                    }
                    break;
                case 13:
                    _canvasManager.EnableEnemyHUD(hit.collider.gameObject);

                    break;
                default:
                    _canvasManager.DisableEnemyHUD();
                    return;

            }
        }
        else
        {
            _canvasManager.DisableEnemyHUD();
        }

        if (hit.collider == null)
        {


        }
        else
        {

            if (hit.collider.gameObject.GetComponent<Item>() != null)
            {
                ItemStats.GetComponent<ItemStatsUI>().selectedItem = hit.collider.gameObject;
                ItemStats.SetActive(false);
                ItemStats.SetActive(true);
            }
            else
            {
                ItemStats.SetActive(false);
            }

        }
    }
  
}
