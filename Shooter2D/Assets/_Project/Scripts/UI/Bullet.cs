﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField] float speed = 10f;
    [SerializeField] float bulletDuration;
    
    public float damage;
    Weapon weapon;
    Player player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
    private void OnDisable()
    {
        CancelInvoke("Desativa");
    }

    void Desativa()
    {
        gameObject.SetActive(false);

    }
    void OnEnable()
    {
        this.GetComponent<Rigidbody2D>().velocity = transform.up * speed;
        Invoke("Desativa", bulletDuration);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Desativa();
            other.GetComponent<Enemy>().TakeDamage(player.RollPlayerDamage());

        }
        if (other.CompareTag("Wall"))
        {
            Desativa();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            //Destroy(this.gameObject, 0.025f);
            Desativa();

        }
    }
}
