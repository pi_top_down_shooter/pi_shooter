﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelection : MonoBehaviour {

    [SerializeField] GameObject bigCharPrefab,smallCharPrefab;

    public string bigCharName,smallCharName;


     void UpdateCharacters()
    {
        if (bigCharName == "Player1")
        {
            bigCharPrefab.GetComponent<PlayerController>().player1 = true;
            smallCharPrefab.GetComponent<PlayerController>().player1 = false;
        }
        else
        {
            bigCharPrefab.GetComponent<PlayerController>().player1 = false;
            smallCharPrefab.GetComponent<PlayerController>().player1 = true;
        }

    }


}
