﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class CanvasManager : MonoBehaviour
{
    static CanvasManager instance;
    public static CanvasManager Instance { get { return instance; } }

    [SerializeField]
    GameObject moreAtributes,enemyHUD;
    [SerializeField]
    Text enemyName;
    [SerializeField]
    Image enemyHpBar;
    public Text levelText, ammoCanvas;
    GameObject player;
    
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;
    public GameObject inventory, ItemStats;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        m_Raycaster = GetComponent<GraphicRaycaster>();
        m_EventSystem = GetComponent<EventSystem>();

    }

    public void EnableEnemyHUD(GameObject enemy)
    {
        enemyName.text = enemy.gameObject.GetComponent<Enemy>().nome + "  " + enemy.gameObject.GetComponent<Enemy>().level;
        float barValue = (float)enemy.gameObject.GetComponent<Enemy>().currentHealth / enemy.gameObject.GetComponent<Enemy>().maxHealth;
        enemyHpBar.fillAmount = barValue;
        enemyHUD.SetActive(false);
        enemyHUD.SetActive(true);
    }

    public void DisableEnemyHUD()
    {
        enemyHUD.SetActive(false);
    }
    public void LevelText()
    {
        levelText.text = "Level " + player.GetComponent<Player>().level;
    }

    public void UpdateAmmoCanvas()
    {
        Weapon _weapon = GameObject.FindGameObjectWithTag("Weapon").GetComponent<Weapon>();
        if (_weapon.maxAmmo >= 100000)
        {
            //score = _weapon.currentAmmo;
            //text.text = " " + score + "/Infinito";
        }
        else
        {

            ammoCanvas.text = " " + _weapon.currentAmmo + "/" + _weapon.maxAmmo;
        }
    }

    private void Update()
    {
        

        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;


        List<RaycastResult> results = new List<RaycastResult>();

        m_Raycaster.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
            if (result.gameObject.name == "More Atributes")
            {
                moreAtributes.SetActive(true);
            }
            else
            {
                moreAtributes.SetActive(false);
                switch (result.gameObject.layer)
                {
                    case 12:
                        if (result.gameObject.GetComponent<InventorySlot>())
                        {
                            if (result.gameObject.GetComponent<InventorySlot>().item != null && result.gameObject.GetComponent<InventorySlot>().buttom.GetComponent<Image>().IsActive())
                            {
                                ItemStats.GetComponent<ItemStatsUI>().selectedItem = result.gameObject.GetComponent<InventorySlot>().item;
                                ItemStats.SetActive(false);
                                ItemStats.SetActive(true);

                            }
                            else
                            {
                                ItemStats.SetActive(false);
                            }
                        }
                        if (result.gameObject.GetComponent<Item>() != null)
                        {
                            ItemStats.GetComponent<ItemStatsUI>().selectedItem = result.gameObject.GetComponent<InventorySlot>().item;
                            ItemStats.SetActive(false);
                            ItemStats.SetActive(true);
                        }
                        if (result.gameObject.GetComponent<CharacterSlot>() != null && result.gameObject.GetComponent<CharacterSlot>().selectedItem != null)
                        {
                            ItemStats.GetComponent<ItemStatsUI>().charSlot = true;
                            ItemStats.GetComponent<ItemStatsUI>().selectedItem = result.gameObject.GetComponent<CharacterSlot>().selectedItem;
                            ItemStats.SetActive(false);
                            ItemStats.SetActive(true);
                        }

                        break;
                    default:
                        ItemStats.SetActive(false);
                        break;
                }
            }

            
        }


    }



}
