﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooling : MonoBehaviour
{

    public int startCount = 20;
    public bool canGrow;
    Weapon weapon;
    public GameObject damageText,muzzleEffect;
    public List<GameObject> damageTextPooledList = new List<GameObject>();
    public List<GameObject> muzzlePooledList = new List<GameObject>();
    // Use this for initialization
    void Start()
    {

        weapon = GameObject.FindGameObjectWithTag("Weapon").GetComponent<Weapon>();

        for (int i = 0; i < startCount; i++)
        {
            weapon.pooledList.Add((GameObject)Instantiate(weapon.bullet));
            weapon.pooledList[weapon.pooledList.Count - 1].SetActive(false);

            damageTextPooledList.Add((GameObject)Instantiate(damageText));
            damageTextPooledList[damageTextPooledList.Count - 1].SetActive(false);

            muzzlePooledList.Add((GameObject)Instantiate(muzzleEffect));
            muzzlePooledList[muzzlePooledList.Count - 1].SetActive(false);

        }
    }


    public GameObject GetPooledObject(List<GameObject> pooledList)
    {


        for (int i = 0; i < pooledList.Count; i++)
        {
            if (!pooledList[i].activeInHierarchy)
            {
                return pooledList[i];
            }
        }

        if (canGrow)
        {
            weapon = GameObject.FindGameObjectWithTag("Weapon").GetComponent<Weapon>();
            pooledList.Add((GameObject)Instantiate(weapon.bullet));
            return pooledList[pooledList.Count - 1];

        }
        return null;
    }

    public GameObject GetPooledDamageText()
    {
        for (int i = 0; i < damageTextPooledList.Count; i++)
        {
            if (!damageTextPooledList[i].activeInHierarchy)
            {
                return damageTextPooledList[i];
            }
        }

        if (canGrow)
        {
            damageTextPooledList.Add((GameObject)Instantiate(damageText));
            return damageTextPooledList[damageTextPooledList.Count - 1];

        }
        return null;
    }

    public GameObject GetPooledMuzzle()
    {
        for (int i = 0; i < muzzlePooledList.Count; i++)
        {
            if (!muzzlePooledList[i].activeInHierarchy)
            {
                return muzzlePooledList[i];
            }
        }

        if (canGrow)
        {
            muzzlePooledList.Add((GameObject)Instantiate(muzzleEffect));
            return muzzlePooledList[muzzlePooledList.Count - 1];

        }
        return null;
    }
}
