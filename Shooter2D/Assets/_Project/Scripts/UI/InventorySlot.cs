﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public class InventorySlot : MonoBehaviour
{
    public GameObject item,buttom;
    [SerializeField]
    PlayerStats _playerStats;
    GameObject player;
    [Header("Atributos do item")]
    //public float damage;
    [SerializeField] CharacterSlot weaponSlot;
    [SerializeField] CharacterSlot ringSlot;
    [SerializeField] CharacterSlot AmuletSlot;
    CharacterSlot itemSlot;
    public Sprite defaultSprite;
    public Manager _manager;
    void Start()
    {
        _manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
        defaultSprite = GetComponent<Image>().sprite;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    

    

    public void OnButtomClick()
    {
        if (player.GetComponent<Player>().level < item.GetComponent<Item>().level)
        {
            return;
        }
        if (!buttom.GetComponent<Image>().enabled)
        {
            return;
        }
        buttom.GetComponent<Image>().enabled = false;
        //weaponSlot.weaponObject = item;

        switch (item.GetComponent<Item>().itemType)
        {
            case Item.ItemType.Weapon:
                weaponSlot.UpdateCurrentPlayerSlot(item);
                weaponSlot.ChangeSprite();
                break;
            case Item.ItemType.Potion:

                break;
            case Item.ItemType.Ring:
                ringSlot.UpdateCurrentPlayerSlot(item);
                ringSlot.ChangeSprite();
                break;
            case Item.ItemType.Amulet:
                AmuletSlot.UpdateCurrentPlayerSlot(item);
                AmuletSlot.ChangeSprite();
                break;
            default:
                break;
        }

        player.GetComponent<Player>().UpdatePlayerStats();
        //remove from list
        //weaponSlot.damage = damage;
        _playerStats.UpdateStats();


    }
    /*
    public void OnPointerExit(PointerEventData eventData)
    {
        _manager.ItemStats.SetActive(false);
    }
    
    
    public void OnPointerEnter(PointerEventData eventData)
    {

        if (item != null && buttom.GetComponent<Image>().IsActive())
        {
            _manager.ItemStats.GetComponent<ItemStatsUI>().selectedItem = item;
            _manager.ItemStats.SetActive(true);

        }
    }
    */
    public void RemoveItemFromInventory()
    {
        GameObject droppedItem = (GameObject)Instantiate(item, player.transform.position, transform.rotation);
        droppedItem.GetComponent<Item>().firstTimeSpawning = false;
        droppedItem.GetComponent<Item>().damageDefault = item.GetComponent<Item>().damageDefault;
        buttom.GetComponent<Image>().enabled = false;
        droppedItem.SetActive(true);
        Destroy(item);
        
    }

    void CheckSlot()
    {
        switch (item.GetComponent<Item>().itemType)
        {
            case Item.ItemType.Weapon:
                itemSlot = weaponSlot;
                break;
            case Item.ItemType.Potion:
                break;
            case Item.ItemType.Ring:
                itemSlot = ringSlot;
                break;
            case Item.ItemType.Amulet:
                itemSlot = AmuletSlot;
                break;
            default:
                break;
        }
        itemSlot.UpdateCurrentPlayerSlot(item);
        itemSlot.ChangeSprite();
    }

   










    /*
    void Activateweapon()
    {
        
        for (int i = 0; i < _changeWeapon.armas.Count; i++)
        {
            _changeWeapon.armas[i].SetActive(false);
        }

        foreach (GameObject weapon in _changeWeapon.armas)
        {
            if (_changeWeapon.armas.Count != 0)
            {
                if (weapon.name == itemName)
                {
                    weapon.GetComponent<Weapon>().damage = weaponSlot.GetComponent<Inventory>().damage;
                    weapon.SetActive(true);
                }
                else
                {
                    print("entrou mas nao tinha arma na lista");
                    GameObject obj = GameObject.Find(itemName);
                    _changeWeapon.armas.Add(item.GetComponent<Item>().playerWeapon);
                    _changeWeapon.armas.Find(x => x.name.Contains(itemName)).SetActive(true);

                }

            }
            else
            {
                print("lista tava vazia");
                GameObject obj = GameObject.Find(itemName);
                _changeWeapon.armas.Add(item.GetComponent<Item>().playerWeapon);
                _changeWeapon.armas.Find(x => x.name.Contains(itemName)).SetActive(true);
            }
            
        }




        
        

    }
    */
}
