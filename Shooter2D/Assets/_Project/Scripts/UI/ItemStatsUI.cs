﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemStatsUI : MonoBehaviour {

    public List<Text> text = new List<Text>();
    public GameObject selectedItem;
    [SerializeField] Text itemNameText,typeText,baseAtribute,level;
    [SerializeField] Color comum, raro, lendario, divino;
    [SerializeField] Image spriteImage,nameBackground,spriteBackground;
    Item _item;
    GameObject player;
    public CharacterSlot[] charSlots;
    [SerializeField]
    GameObject equippedItemStats;
    [SerializeField]
    bool isEquippedItem ;
    public bool charSlot;
    private void OnDisable()
    {
        if (!isEquippedItem)
        {
            equippedItemStats.SetActive(false);
        }

    }
    private void OnEnable()
    {

        player = GameObject.FindGameObjectWithTag("Player");


        if (!isEquippedItem)
        {

            _item = selectedItem.GetComponent<Item>();
            equippedItemStats.SetActive(false);

            if (!charSlot)
            {
                foreach (var item in charSlots)
                {
                    if (item.selectedItem && item.selectedItem.GetComponent<Item>().itemType == _item.itemType)
                    {
                        equippedItemStats.GetComponent<ItemStatsUI>()._item = item.selectedItem.GetComponent<Item>();
                        equippedItemStats.SetActive(true);

                    }
                }
            }

        }



        switch (_item.itemRarity)
        {
            case "Comum":
                itemNameText.color = comum;
                typeText.color = comum;
                nameBackground.color = new Color(comum.r,comum.g,comum.b,0.10f);
                spriteBackground.color = new Color(comum.r, comum.g, comum.b, 0.10f);
 
                break;
            case "Raro":
                itemNameText.color = raro;
                typeText.color = raro;
                nameBackground.color = new Color(raro.r, raro.g, raro.b, 0.10f);
                spriteBackground.color = new Color(raro.r, raro.g, raro.b, 0.10f);
                break;
            case "Lendario":
                itemNameText.color = lendario;
                typeText.color = lendario;
                nameBackground.color = new Color(lendario.r, lendario.g, lendario.b, 0.10f);
                spriteBackground.color = new Color(lendario.r, lendario.g, lendario.b, 0.10f);
                break;
            case "Divino":
                itemNameText.color = divino;
                typeText.color = divino;
                nameBackground.color = new Color(divino.r, divino.g, divino.b, 0.10f);
                spriteBackground.color = new Color(divino.r, divino.g, divino.b, 0.10f);
                break;
            default:
                break;
        }

        ClearText();
        itemNameText.text = _item.itemName ;
        typeText.text = _item.itemType.ToString() + " " + _item.itemRarity;

        level.text = "Level " + _item.level;
        spriteImage.sprite = _item.GetComponent<SpriteRenderer>().sprite;
        switch (_item._atributeType)
        {
            case Item.AtributeType.Damage:
                baseAtribute.text = "Damage " + _item.damageDefault;
                break;
            case Item.AtributeType.CriticalDamage:
                baseAtribute.text = "Critical Damage " + (_item.critDmgDefault * 100).ToString("F1") + "%";
                break;
            case Item.AtributeType.CriticalChance:
                baseAtribute.text = "Critical Chance " + (_item.critChanceDefault * 100).ToString("F1") + "%";
                break;
            case Item.AtributeType.LifeSteal:
                baseAtribute.text = "Life Steal " + (_item.lifeStealDefault * 100).ToString("F1") + "%" ;
                break;
            case Item.AtributeType.MovementSpeed:
                baseAtribute.text = "Movement Speed " + _item.msDefault;
                break;
            case Item.AtributeType.Regeneration:
                baseAtribute.text = "Regen " + (_item.regenDefault).ToString("F1") + "%";
                break;
            case Item.AtributeType.Armor:
                baseAtribute.text = "Armor " + _item.armorDefault;
                break;
            case Item.AtributeType.ExtraExp:
                baseAtribute.text = "Extra Exp " + (_item.expDefault * 100).ToString("F1") + "%";
                break;
            case Item.AtributeType.ExtraGold:
                baseAtribute.text = "Extra Gold " + (_item.goldDefault * 100).ToString("F1") + "%";
                break;
            default:
                break;
        }
        


        for (int i = 0; i < _item.atributosSelecionados.Count; i++)
        {
            switch (_item.atributosSelecionados[i])
            {
                case "Critical Damage": text[i].text = _item.atributosSelecionados[i] + "  " + (_item.valorDosAtributos[i] * 100).ToString("F1") + "%";
                    break;
                case "Critical Chance":
                    text[i].text = _item.atributosSelecionados[i] + "  " + (_item.valorDosAtributos[i] * 100).ToString("F1") + "%";
                    break;
                case "Extra Exp":
                    text[i].text = _item.atributosSelecionados[i] + "  " + (_item.valorDosAtributos[i] * 100).ToString("F1") + "%";
                    break;
                case "Extra Gold":
                    text[i].text = _item.atributosSelecionados[i] + "  " + (_item.valorDosAtributos[i] * 100).ToString("F1") + "%";
                    break;
                case "Life Steal":
                    text[i].text = _item.atributosSelecionados[i] + "  " + (_item.valorDosAtributos[i] * 100).ToString("F1") + "%";
                    break;
                case "Regen":
                    text[i].text = _item.atributosSelecionados[i] + "  " + (_item.valorDosAtributos[i]).ToString("F1") + "%";
                    break;
                default:
                    text[i].text = _item.atributosSelecionados[i] + "  " + _item.valorDosAtributos[i];
                    break;
            }


            
        }

        if (_item.level > player.GetComponent<Player>().level)
        {
            level.color = Color.red;
        }
        else
        {
            level.color = Color.white;
        }
        charSlot = false;
    }

    void ClearText()
    {
        for (int i = 0; i < text.Count; i++)
        {
            text[i].text = "";
        }
    }

}
