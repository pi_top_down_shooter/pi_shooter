﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerStats : MonoBehaviour {

    public Text level;
    public Text gold;
    public Text damage,critChance,critDmg,armor,speed,lifeSteal,extraExp,extraGold,regen;
    public Text nome;
    public GameObject playerPrefab;

    private void Awake()
    {
        playerPrefab = GameObject.FindGameObjectWithTag("Player");
        nome.text = "" + playerPrefab.GetComponent<Player>().nome;

    }

    private void OnEnable()
    {
        
        UpdateStats();
    }

    public void UpdateStats()
    {
        level.text = "" + playerPrefab.GetComponent<Player>().level;
        gold.text = "" + playerPrefab.GetComponent<Player>().currentGold;
        damage.text = "" + playerPrefab.GetComponent<Player>().currentDamage;
        critChance.text = "" + playerPrefab.GetComponent<Player>().a_critChance * 100 + "%";
        critDmg.text = "" + playerPrefab.GetComponent<Player>().a_critDamage * 100 + "%";
        armor.text = "" + playerPrefab.GetComponent<Player>().a_armor;
        speed.text = "" + playerPrefab.GetComponent<Player>().speed;
        lifeSteal.text = "" + playerPrefab.GetComponent<Player>().a_lifeSteal*100 + "%";
        extraExp.text = "" + playerPrefab.GetComponent<Player>().a_extraExp * 100 + "%";
        extraGold.text = "" + playerPrefab.GetComponent<Player>().a_extraGold * 100 + "%";
        regen.text = "" + playerPrefab.GetComponent<Player>().a_regen;

    }



   

}


