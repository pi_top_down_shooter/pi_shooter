﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    [System.Flags]
    public enum ItemType
    {
        Weapon = 1,
        Potion = 2,
        Ring = 3,
        Amulet = 4
    }
    public enum AtributeType
    {
        Damage, CriticalDamage,CriticalChance,LifeSteal,MovementSpeed,Regeneration ,Armor ,ExtraExp,ExtraGold,Normal
        

    }
    public AtributeType _atributeType;
    public ItemType itemType;
    public int level, enumIndexMin, enumIndexMax;
    List<string> atributos = new List<string>();
    public List<string> atributosSelecionados = new List<string>();
    public List<float> valorDosAtributos = new List<float>();

    public GameObject inventory;
    public string itemName,itemRarity;
    ChangeWeapon weaponList;
    GameObject playerWeapon;
    GameObject itemStats;
    Manager _manager;
    public Inventory _inventory;
    public bool firstTimeSpawning = true;
    int numberOfAtributes;
    [HideInInspector]
    public float damageDefault, regenDefault, critDmgDefault, critChanceDefault, lifeStealDefault, msDefault, armorDefault,expDefault,goldDefault;
    //--------------------------------------------------------------------
    // variaveis de atributo
    public float a_damage, a_critDamage, a_critChance, a_lifeSteal , a_bulletCapacity , a_regen, a_armor, a_extraExp, a_extraGold, a_movementSpeed, a_spellDamage;

    private void Start()
    {
       
        itemStats = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>().ItemStats;
        

        _manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
        _inventory = _manager.inventory.GetComponent<Inventory>();
        //GetReferencedObject();


        if (firstTimeSpawning)
        {

            itemRarity = RollRarity();
            Atributes();
            SortAtributes();

            if (itemType == ItemType.Weapon)
            {
                _atributeType = AtributeType.Damage;
            }

                switch (_atributeType)
                {

                    case AtributeType.Normal:

                        _atributeType = (AtributeType)Random.Range(enumIndexMin, enumIndexMax);
                        break;
                    case AtributeType.Damage:
                        damageDefault = Damage();
                        CalculateWeaponDamage();
                        break;
                    case AtributeType.CriticalDamage:
                        critDmgDefault = CriticalDamage();
                        break;
                    case AtributeType.CriticalChance:
                        critChanceDefault = CriticalChance();
                        break;
                    case AtributeType.LifeSteal:
                        lifeStealDefault = LifeSteal();
                        break;
                    case AtributeType.MovementSpeed:
                        msDefault = MovementSpeed();
                        break;
                    case AtributeType.Regeneration:
                        regenDefault = Regen();
                        break;
                    case AtributeType.Armor:
                        armorDefault = Armor();
                        break;
                    case AtributeType.ExtraExp:
                        expDefault = ExtraExp();
                        break;
                    case AtributeType.ExtraGold:
                        goldDefault = ExtraGold();
                        break;
                    default:
                        break;
                }

            firstTimeSpawning = false;
        }

        
    }

    public void _OnMouseOver()
    {
        itemStats.GetComponent<ItemStatsUI>().selectedItem = gameObject;
        itemStats.SetActive(true);
    }


     public void _OnMouseDown()
    {
        
        _inventory.AddItemToIventory(gameObject);
        this.gameObject.SetActive(false);
        itemStats.SetActive(false);
        

    }
    private void OnMouseExit()
    {
        itemStats.SetActive(false);
    }

    string RollRarity()
    {
        // comum 1, raro 2, lendario 3, divino 4
        float rarity = Random.Range(0f, 1f);
        Debug.Log(rarity);
        if (rarity > 0.10f)
        {
            numberOfAtributes = 1;
            return "Comum";
        }
        else if (rarity > 0.025f && rarity <=0.10f)
        {
            numberOfAtributes = 2;
            return "Raro";
        }
        else if (rarity > 0.015f && rarity <= 0.025f)
        {
            numberOfAtributes = 3;
            return "Lendario";
        }
        else
        {
            numberOfAtributes = 5;
            return "Divino";
        }
        
    }

    

    void Atributes()
    {
      

        switch (itemType)
        {
            case ItemType.Weapon:
                enumIndexMin = 1;
                enumIndexMax = 4;

                atributos.Clear();
                atributos.Add("Damage");
                atributos.Add("Life Steal");
                atributos.Add("Critical Damage");
                atributos.Add("Critical Chance");
                atributos.Add("Armor");
                atributos.Add("Bullet Capacity");

                break;
            case ItemType.Potion:
                break;
            case ItemType.Ring:
                enumIndexMin = 0;
                enumIndexMax = 9;
                atributos.Clear();
                atributos.Add("Damage");
                atributos.Add("Movement Speed");
                atributos.Add("Bullet Capacity");
                atributos.Add("Spell Damage");
                atributos.Add("Extra Exp");
                atributos.Add("Extra Gold");
                atributos.Add("Regen");
                break;
            case ItemType.Amulet:
                enumIndexMin = 0;
                enumIndexMax = 9;
                atributos.Clear();
                atributos.Add("Damage");
                atributos.Add("Movement Speed");
                atributos.Add("Critical Damage");
                atributos.Add("Critical Chance");
                atributos.Add("Spell Damage");
                atributos.Add("Extra Exp");
                atributos.Add("Extra Gold");
                atributos.Add("Regen");
                atributos.Add("Life Steal");
                atributos.Add("Armor");
                atributos.Add("Bullet Capacity");

                break;
            default:
                break;
        }
    }

    void CalculateWeaponDamage()
    {

        foreach (GameObject item in Player.Instance.GetComponentInChildren<ChangeWeapon>().armas)
        {
            if (item.name == itemName)
            {

                damageDefault *= (1 / item.GetComponent<Weapon>().fireRate);
                if (itemName == "Shotgun")
                {
                    damageDefault *= 5;
                }
                
            }
        }
        
    }


    void SortAtributes()
    {

        atributosSelecionados.Clear();
 
        for (int i = 0; i < numberOfAtributes; i++)
        {

            int a = Random.Range(0, atributos.Count);
            string name = atributos[a];           

            if (!atributosSelecionados.Contains(name))
            {
                atributosSelecionados.Add(name);
                atributos.Remove(name);
            }
            else
            {
                
                print("atributos iguais");
                numberOfAtributes++;
                //SortAtributes();
                
            }

            switch (name)
            {
                case "Damage":
                    a_damage = Damage();                    
                    valorDosAtributos.Add(a_damage);

                    break;
                case "Movement Speed":
                    a_movementSpeed = MovementSpeed();
                    valorDosAtributos.Add(a_movementSpeed);
                    break;
                case "Critical Damage":
                    a_critDamage = CriticalDamage();
                    valorDosAtributos.Add(a_critDamage);
                    break;
                case "Critical Chance":
                    a_critChance = CriticalChance();
                    valorDosAtributos.Add(a_critChance);
                    break;
                case "Spell Damage":
                    a_spellDamage = SpellDamage();
                    valorDosAtributos.Add(a_spellDamage);
                    break;
                case "Extra Gold":
                    a_extraGold = ExtraGold();
                    valorDosAtributos.Add(a_extraGold);
                    break;
                case "Extra Exp":
                    a_extraExp = ExtraExp();
                    valorDosAtributos.Add(a_extraExp);
                    break;
                case "Regen":
                    a_regen = Regen();
                    valorDosAtributos.Add(a_regen);
                    break;
                case "Life Steal":
                    a_lifeSteal = LifeSteal();
                    valorDosAtributos.Add(a_lifeSteal);
                    break;
                case "Armor":
                    a_armor = Armor();
                    valorDosAtributos.Add(a_armor);
                    break;
                case "Bullet Capacity":
                    a_bulletCapacity = BulletCapacity();
                    valorDosAtributos.Add(a_bulletCapacity);
                    break;


                default:
                    print("nao teve atributos sorteado");
                    break;
            }
        }
        

    }
    

    float Damage()
    {

        float damage;
 
        switch (itemRarity)
        {
            
            case "Comum": damage = Random.Range(1 + (level * 0.25f), 1 + (level * 0.40f));

                break;
            case "Raro":
                damage = Random.Range(1.25f + (level * 0.425f), 1.3f + (level * 0.5f));
                break;
            case "Lendario":
                damage = Random.Range(1.75f + (level * 0.6f), 2f + (level * 0.7f));
                break;
            case "Divino":
                damage = Random.Range(2.5f + (level * 0.85f), 2.75f + (level * 1f));
                break;

            default:
                damage = 0;
                break;
               
        }
        return RoundNumber(damage);
    }

    float MovementSpeed()
    {
        float movementSpeed;
        switch (itemRarity)
        {
            case "Comum":
                movementSpeed = Random.Range(level*0.15f + level/11.5f, level * 0.175f + level / 11f);

                break;
            case "Raro":
                movementSpeed = Random.Range(level * 0.175f + level / 10.5f, level * 0.2f + level / 10f);
                break;
            case "Lendario":
                movementSpeed = Random.Range(level * 0.2f + level / 10f, level * 0.25f + level / 9.5f);
                break;
            case "Divino":
                movementSpeed = Random.Range(level * 0.3f + level / 9f, level * 0.35f + level / 8f);
                break;

            default:
                movementSpeed = 0;
                break;

        }
        return RoundNumber(movementSpeed);
    }

    float CriticalDamage()
    {
        float criticalDamage;
        switch (itemRarity)
        {
            case "Comum":
                criticalDamage = Random.Range(10f, 15f);

                break;
            case "Raro":
                criticalDamage = Random.Range(15f, 25f);
                break;
            case "Lendario":
                criticalDamage = Random.Range(25f, 45f);
                break;
            case "Divino":
                criticalDamage = Random.Range(45f, 70f);
                break;

            default:
                criticalDamage = 0;
                break;

        }
        return RoundNumber(criticalDamage / 100);
    }

    float CriticalChance()
    {
        float criticalChance;
        switch (itemRarity)
        {
            case "Comum":
                criticalChance = Random.Range(2.5f, 5f);

                break;
            case "Raro":
                criticalChance = Random.Range(5.5f, 7.5f);
                break;
            case "Lendario":
                criticalChance = Random.Range(8f, 10f);
                break;
            case "Divino":
                criticalChance = Random.Range(13.5f, 17.5f);
                break;

            default:
                criticalChance = 0;
                break;

        }
        return RoundNumber(criticalChance / 100);
    }

    float SpellDamage()
    {
        float spellDamage;
        switch (itemRarity)
        {
            case "Comum":
                spellDamage = Random.Range(1f, 5f);

                break;
            case "Raro":
                spellDamage = Random.Range(5f, 10f);
                break;
            case "Lendario":
                spellDamage = Random.Range(10f, 15f);
                break;
            case "Divino":
                spellDamage = Random.Range(15f, 20f);
                break;

            default:
                spellDamage = 0;
                break;

        }
        return RoundNumber(spellDamage / 100);
    }

    float ExtraGold()
    {
        float extraGold;
        switch (itemRarity)
        {
            case "Comum":
                extraGold = Random.Range(1f, 2f);

                break;
            case "Raro":
                extraGold = Random.Range(2.25f, 4.5f);
                break;
            case "Lendario":
                extraGold = Random.Range(5f, 7f);
                break;
            case "Divino":
                extraGold = Random.Range(8f, 9.75f);
                break;

            default:
                extraGold = 0;
                break;

        }
        return RoundNumber(extraGold / 100);
    }

    float ExtraExp()
    {
        float extraExp;
        switch (itemRarity)
        {
            case "Comum":
                extraExp = Random.Range(0.75f, 1.75f);

                break;
            case "Raro":
                extraExp = Random.Range(2f, 3.25f);
                break;
            case "Lendario":
                extraExp = Random.Range(4.5f, 5.75f);
                break;
            case "Divino":
                extraExp = Random.Range(7f, 8.75f);
                break;

            default:
                extraExp = 0;
                break;

        }
        return RoundNumber(extraExp / 100);
    }

    float Regen()
    {
        float regen;
        switch (itemRarity)
        {
            case "Comum":
                regen = Random.Range(0.1f, 0.2f);

                break;
            case "Raro":
                regen = Random.Range(0.25f, 0.35f);
                break;
            case "Lendario":
                regen = Random.Range(0.5f, 0.75f);
                break;
            case "Divino":
                regen = Random.Range(1f, 1.5f);
                break;

            default:
                regen = 0;
                break;

        }
        return RoundNumber(regen);
    }

    float LifeSteal()
    {
        float lifeSteal;
        switch (itemRarity)
        {
            case "Comum":
                lifeSteal = Random.Range(2f, 4f);

                break;
            case "Raro":
                lifeSteal = Random.Range(4f, 6f);
                break;
            case "Lendario":
                lifeSteal = Random.Range(6f, 8f);
                break;
            case "Divino":
                lifeSteal = Random.Range(8f, 10f);
                break;

            default:
                lifeSteal = 0;
                break;

        }
        return RoundNumber(lifeSteal/100);
    }

    float Armor()
    {
        float armor;
        switch (itemRarity)
        {
            case "Comum":
                armor = Random.Range(1f, 1.75f);

                break;
            case "Raro":
                armor = Random.Range(2f, 3.75f);
                break;
            case "Lendario":
                armor = Random.Range(4f, 6f);
                break;
            case "Divino":
                armor = Random.Range(7f, 9.5f);
                break;

            default:
                armor = 0;
                break;

        }
        return  RoundNumber(armor);
    }

    float BulletCapacity()
    {
        float bulletCapacity;
        switch (itemRarity)
        {
            case "Comum":
                bulletCapacity = Random.Range(20, 51);

                break;
            case "Raro":
                bulletCapacity = Random.Range(50, 81);
                break;
            case "Lendario":
                bulletCapacity = Random.Range(80, 111);
                break;
            case "Divino":
                bulletCapacity = Random.Range(110, 151);
                break;

            default:
                bulletCapacity = 0;
                break;

        }

        return bulletCapacity;
    }

    float RoundNumber(float num)
    {
        num *= 100;
        num = Mathf.RoundToInt(num);
        num /= 100;
        return num;
    }


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" && col.gameObject.GetComponent<PlayerController>().gettingItem)
        {
            if (gameObject == col.gameObject.GetComponent<PlayerController>().itemPosition)
            {
                col.gameObject.GetComponent<PlayerController>().gettingItem = false;
                _OnMouseDown();
            }

        }
    }
}
