﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuffle {

    public static Dictionary<string, List<int>> valores = new Dictionary<string, List<int>>();


    public static int Range(string tag,int min, int max)
    {
        if (!valores.ContainsKey(tag))
        {
            valores.Add(tag, new List<int>());

        }
        if (valores[tag].Count == 0)
        {
            valores[tag].Clear();
            for (int i = min; i < max; i++)
            {
                valores[tag].Add(i);
            }
        }
        int index = Random.Range(0, valores[tag].Count - 1);
        int result = valores[tag][index];
        valores[tag].RemoveAt(index);
        if (result<min)
        {
            return Range(tag, min, max);
        }
        if (result > max)
        {
            return Range(tag, min, max);
        }
        return result;
    }
}
