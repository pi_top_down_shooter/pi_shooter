﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingBeing : MonoBehaviour {
    public float maxHealth = 10;
    public float currentHealth = 0;
    public float startSpeed = 5;
    public float speed;
    public float damage = 0;
    public bool isAlive = true;
    public int level;
    public int gold;
    public int exp;
    public string nome;


    void Awake() {
        HitPointsCalculation();
        speed = startSpeed;
    }

    protected virtual void HitPointsCalculation() {
        currentHealth = maxHealth;
        isAlive = true;
    }

    public virtual void TakeDamage(float amount) {
        if (!isAlive || amount <= 0) 
            return;


        currentHealth -= amount; 
        if (currentHealth <= 0)
            Death();


    }

    public void Healing(float hitPointBonus) {
        if (!isAlive)
            return;

        currentHealth += hitPointBonus;
        //hitPointsCurrent = Mathf.Min(hitPointsMax, hitPointsCurrent);
        currentHealth = Mathf.Clamp(currentHealth, currentHealth, maxHealth);
        
    }


    public virtual void Death() {
        currentHealth = Mathf.Max(0, currentHealth);
        isAlive = false;
    }
}
