﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Subject {  MULTIPLAYER,
                       INTERFACE,
                       MARKETING,
                       ROTEIRO,
                       ANIMACAO_AVANCADA,
                       PI }

public class Student : MonoBehaviour {

	//Nome
    //Matricula
    //Disciplina (vem de um enum Disciplians)
    //otimismo - vou passar na disciplina

    public string studentName = "Joao";
    public int studentID = 123456;
    public bool passThisCourse = true;
    public Subject subject = Subject.MULTIPLAYER;
}
