﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gear : MonoBehaviour {

	public string gearName = "Equipamento 000";
    public float weight = 1;
    public int price = 10;
    public bool equipped = false;
}
