﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {
    [SerializeField] Transform player;
    bool canFollow;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (canFollow)
        {
            transform.position = player.position;
        }
        


	}

    public void Shake()
    {
        GetComponent<Animator>().SetTrigger("Shake");
        canFollow = false;
    }

    public void StopShake()
    {
        canFollow = true;
    }
}
