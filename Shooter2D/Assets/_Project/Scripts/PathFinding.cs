﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{






    public List<GameObject> tilesTotal = new List<GameObject>();
    public List<GameObject> tilesToGo = new List<GameObject>();
    public List<int> layers = new List<int>();


    private void Start()
    {

        layers.Add(8);
        layers.Add(15);
        RestartTiles();
    }

    public void RestartTiles()
    {
        tilesToGo.Clear();
        for (int i = 0; i < tilesTotal.Count; i++)
        {
            tilesToGo.Add(tilesTotal[i]);
        }
    }
}


