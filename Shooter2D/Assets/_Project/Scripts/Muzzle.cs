﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muzzle : MonoBehaviour {

    [SerializeField] float speed;


    void OnEnable()
    {
        this.GetComponent<Rigidbody2D>().velocity = transform.up * speed;
    }

    public void Desativa()
    {
        gameObject.SetActive(false);
    }

}
