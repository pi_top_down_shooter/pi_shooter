﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterRoom : MonoBehaviour {

    [SerializeField] Room room;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player") && room.state != Room.SpawnState.COMPLETED)
        {
            /*
            print("entrou na room collider");
            GameObject[] cams = GameObject.FindGameObjectsWithTag("Room");
            if (cams != null)
            {
                Debug.Log(cams.Length);
            }
            
            for (int i = 0; i < cams.Length -1; i++)
            {
                if (cams[i].GetComponent<Room>() && cams[i] != gameObject)
                {
                    cams[i].GetComponent<Room>().vCamera.SetActive(false);
                }

                
            }
            
            room.vCamera.SetActive(true);
            room.cinemachine = room.vCamera.GetComponent<CinemachineVirtualCameraBase>();
            room.cinemachine.Follow = col.gameObject.transform;
            */
            room.canSpawn = true;
            RoomManager.Instance.roomAtual = room.gameObject;
        }
    }
}
