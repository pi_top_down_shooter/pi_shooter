﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;



public class Room : MonoBehaviour
{
    public Entrance entrance;
    public Exit exit;
    public enum SpawnState { SPAWNING, WAITING, COUNTING, COMPLETED };
    public enum Entrance { NORTH, RIGHT, SOUTH, LEFT }
    public enum Exit { NORTH, RIGHT, SOUTH, LEFT }
    public GameObject enemy;
    public int count, startCount;
    private float searchCountdown = 1f;

    public bool canSpawn;
    public Transform pos;
    public GameObject doors;
    public SpawnState state = SpawnState.COUNTING;
    RaycastHit hit;
    Ray ray; 
    public int groundObjectsCount, wallObjectsCount;
    public GameObject[] arma;
    public GameObject[] groundObjects,wallObjects,objectPositions;
    public Transform scanRoomPos;
    [Header("Nao mexer")]
    public Transform[] waypoints;
    public Transform[] saidas;
    public List<Vector2> availableGroundPlaces = new List<Vector2>();
    public List<Vector2> availableWallPlaces = new List<Vector2>();

    public Tilemap tileMapGround;

    public Tilemap tileMapWall;

    public Transform offset;
    void Start()
    {
        availableGroundPlaces.Clear();
        availableWallPlaces.Clear();
        state = SpawnState.COUNTING;
        GenerateRoom();
        count = startCount;
        canSpawn = false;
       // vCamera.SetActive(true);
    }
    private void FixedUpdate()
    {
        
    }
    void Update()
    {


        if (state == SpawnState.WAITING)
        {
            if (!EnemyIsAlive())
            {

                if (count > 0)
                {
                    state = SpawnState.COUNTING;
                    canSpawn = true;
                }
                else
                {
                    WaveCompleted();
                    Debug.Log("Wave copleted!");
                }

            }

            else
            {
                return;
            }
        }
            if (canSpawn)
            {

                if (state != SpawnState.SPAWNING && count > 0)
                {
                    SpawnWave();

            }
            }
        
    }

    void WaveCompleted()
    {
        state = SpawnState.COMPLETED;
        doors.SetActive(false);
        saidas[0].GetComponent<LeaveRoom>().Spawn();
    }

    void SpawnWave()
    {

        doors.SetActive(true);
        canSpawn = false;
        state = SpawnState.SPAWNING;
        SpawnEnemy();
        count = count - 8;
        state = SpawnState.WAITING;

    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }


        return true;
    }
    public void SpawnEnemy()
    {
        
        for (int i = 0; i < Mathf.Min(8,count); i++)
        {
            int j = Random.Range(0, availableGroundPlaces.Count);
            pos.position = availableGroundPlaces[j];
            Instantiate(enemy, pos.position, pos.rotation);


        }

    }


    public void GenerateRoom()
    {

        // gerar objetos chao
        for (int i = 0; i < groundObjectsCount; i++)
        {
            int j = Random.Range(0, objectPositions.Length);
            int k = Random.Range(0, groundObjects.Length);
            if (objectPositions[j] != null)
            {
                Instantiate(groundObjects[k], objectPositions[j].transform.position, transform.rotation);
            }
            
        }
        ///////////////////////////////////////////////////////////////////////////////////////



        // lista tiles de chao
 

        for (int n = tileMapGround.cellBounds.xMin; n < tileMapGround.cellBounds.xMax; n++)
        {
            for (int p = tileMapGround.cellBounds.yMin; p < tileMapGround.cellBounds.yMax; p++)
            {
                Vector3Int localPlace = (new Vector3Int(n, p, (int)tileMapGround.transform.position.y));


                Vector2 place = tileMapGround.CellToWorld(localPlace);
                Vector2 direction = Vector2.down;
                float distance = 1.0f;

                RaycastHit2D hit = Physics2D.Raycast(place, direction, distance, 9);
                if (tileMapGround.HasTile(localPlace))
                {
                    if (!hit.collider)
                    {
                        availableGroundPlaces.Add(place);
                    }
                }
               
            }
        }
        if (availableGroundPlaces.Count == 0)
        {
                foreach (var position in tileMapGround.cellBounds.allPositionsWithin)
                {
                Vector2 place = tileMapGround.CellToWorld(position);
                RaycastHit2D hit = Physics2D.Raycast(place, Vector2.down, 1, 9);
                if (tileMapGround.HasTile(position))
                {
                    if (!hit.collider)
                    {
                        availableGroundPlaces.Add(place);
                    }
                }
                /*
                if (tileMapGround.HasTile(position))
                    {
                        Vector2 place2 = tileMapGround.CellToWorld(position);
                        availableGroundPlaces.Add(place2);
                    }
*/
                }
            
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        




        //gerar arma
        int a = Random.Range(0, arma.Length);
        int b = Random.Range(0, availableGroundPlaces.Count);
        Instantiate(arma[a], availableGroundPlaces[b], transform.rotation);
        ///////////////////////////////////////////////////////////////////////////////////////

        //lista tiles de parede




        for (int n = tileMapWall.cellBounds.xMin; n < tileMapWall.cellBounds.xMax; n++)
        {
            for (int p = tileMapWall.cellBounds.yMin; p < tileMapWall.cellBounds.yMax; p++)
            {
                Vector3Int localPlace = (new Vector3Int(n, p, (int)tileMapWall.transform.position.y));


                Vector2 place = tileMapWall.CellToWorld(localPlace);
                Vector2 direction = Vector2.down;
                float distance = 1.0f;

                RaycastHit2D hit = Physics2D.Raycast(place, direction, distance, 8);
                if (tileMapWall.HasTile(localPlace))
                {
                    if (!hit.collider)
                    {
                        availableWallPlaces.Add(place);
                    }
                }
                
            }
        }
        if (availableWallPlaces.Count == 0)
        {

                foreach (var position in tileMapWall.cellBounds.allPositionsWithin)
                {
                    if (tileMapWall.HasTile(position))
                    {
                        Vector2 place2 = tileMapWall.CellToWorld(position);
                        availableWallPlaces.Add(place2);
                    }

                }
            
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        
        // gerar objetos parede
        for (int i = 0; i < wallObjectsCount; i++)
        {
            int j = Random.Range(0, availableWallPlaces.Count);            
            int k = Random.Range(0, wallObjects.Length);
            Vector2 pos = new Vector2(availableWallPlaces[j].x + 0.5f, availableWallPlaces[j].y+ 0.5f);
            Instantiate(wallObjects[k], pos, transform.rotation);
            availableWallPlaces.Remove(availableWallPlaces[j]);
        }

        
        
    }



}

