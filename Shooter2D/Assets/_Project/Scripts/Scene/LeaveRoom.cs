﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveRoom : MonoBehaviour {

    public Room _room;
    public GameObject roomToInstanciate;
    float x1, y1;
    float x2, y2;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player") && _room.state == Room.SpawnState.COMPLETED)
        {
            _room.doors.SetActive(true);

            //Spawn();
        }
    }
    private void Update()
    {
        
    }

    public  void Spawn()
    {

        _room.offset.position = _room.waypoints[1].position + _room.GetComponent<Transform>().position;
        //raycast retornou se colidiu
        switch (_room.exit)
        {
             
            case Room.Exit.NORTH:
                // if (!colidiu)
                Debug.Log(_room.exit);
                roomToInstanciate = RoomManager.Instance.ChooseRoom("south");
                break;
            case Room.Exit.RIGHT:
                Debug.Log(_room.exit);
                roomToInstanciate = RoomManager.Instance.ChooseRoom("left");
                break;
            case Room.Exit.SOUTH:
                Debug.Log(_room.exit);
                roomToInstanciate = RoomManager.Instance.ChooseRoom("north");
                break;
            case Room.Exit.LEFT:
                
                roomToInstanciate = RoomManager.Instance.ChooseRoom("right");
                break;
            default:
                
                break;
        }
        _room.offset.position = _room.waypoints[1].position + _room.GetComponent<Transform>().position;
        
        

        x1 = _room.waypoints[1].transform.position.x;
        x2 = roomToInstanciate.transform.position.x - roomToInstanciate.GetComponent<Room>().waypoints[0].transform.position.x;
        y1 = _room.waypoints[1].transform.position.y;
        y2 = roomToInstanciate.transform.position.y - roomToInstanciate.GetComponent<Room>().waypoints[0].transform.position.y;
        Vector2 pos = new Vector2(x1 + x2, y1 + y2);
        GameObject newRoom = (GameObject)Instantiate(roomToInstanciate, pos, _room.offset.rotation) as GameObject;
        newRoom.GetComponent<Room>().doors.SetActive(false);

    }


    void SpawnRoom()
    {
        GameObject room = RoomManager.Instance.ChoseRoom();
        for (int i = 0; i < _room.saidas.Length -1; i++)
        {
            if (gameObject.name == _room.saidas[i].name)
            {


                _room.offset.position = _room.waypoints[i].position + _room.GetComponent<Transform>().position;
            }
        }
        Vector2 spawn = new Vector2(25, 25);
        
        room.GetComponent<Room>().doors.SetActive(false);


        if (gameObject.name == "LeaveDoorPointRight")
        {
            x1 = _room.waypoints[1].transform.position.x;
            x2 = room.transform.position.x - room.GetComponent<Room>().waypoints[3].transform.position.x;
            y1 = _room.waypoints[1].transform.position.y;
            y2 = room.transform.position.y - room.GetComponent<Room>().waypoints[3].transform.position.y;

            Vector2 pos = new Vector2(x1+x2,y1 + y2);


            
            GameObject newRoom = (GameObject)Instantiate(room,pos , _room.offset.rotation) as GameObject;
        }
        if (gameObject.name == "LeaveDoorPointLeft")
        {
            x1 = _room.waypoints[3].transform.position.x;
            x2 = room.transform.position.x - room.GetComponent<Room>().waypoints[1].transform.position.x;
            y1 = _room.waypoints[3].transform.position.y;
            y2 = room.transform.position.y - room.GetComponent<Room>().waypoints[1].transform.position.y;
            Vector2 pos = new Vector2(x1 + x2, y1+y2);
            GameObject newRoom = (GameObject)Instantiate(room, pos, _room.offset.rotation) as GameObject;
        }

        if (gameObject.name == "LeaveDoorPointUp")
        {
            x1 = _room.waypoints[0].transform.position.x;
            x2 = room.transform.position.x - room.GetComponent<Room>().waypoints[2].transform.position.x;
            y1 = _room.waypoints[0].transform.position.y;
            y2 = room.transform.position.y - room.GetComponent<Room>().waypoints[2].transform.position.y;
            Vector2 pos = new Vector2(x1+x2, y1 + y2);
            GameObject newRoom = (GameObject)Instantiate(room, pos, _room.offset.rotation) as GameObject;
        }

        if (gameObject.name == "LeaveDoorPointDown")
        {
            y1 = _room.waypoints[2].transform.position.y;
            y2 = room.transform.position.y - room.GetComponent<Room>().waypoints[0].transform.position.y;
            x1 = _room.waypoints[2].transform.position.x;
            x2 = room.transform.position.x - room.GetComponent<Room>().waypoints[0].transform.position.x;
            Vector2 pos = new Vector2(x1 + x2, y1+y2);
            GameObject newRoom = (GameObject)Instantiate(room, pos, _room.offset.rotation) as GameObject;
        }



    }








}
