﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour {


   

    public GameObject[] rooms;
    static RoomManager instance;
    public static RoomManager Instance { get { return instance; } }
    public GameObject[] leftEntranceRooms, rightEntranceRooms, southEntranceRooms, northEntranceRooms;
    int index;
    GameObject roomToinstanciate;
    public GameObject roomAtual;
    RaycastHit2D hit;
    Vector2 v_saidaDesejada;
    Vector2 _v;
        public string saidaDesejada = "NULL";
    public int count = 0;
    public List<GameObject> gridsUsadas = new List<GameObject>();
    // Use this for initialization
    void Awake () {
        instance = this;


    }

    private void Start()
    {
    }

    // Update is called once per frame
    void Update () {
		
	}



    public GameObject ChooseRoom(string entrance)
    {

        // eu preciso de uma saida desejada?
        count = 0;
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {                
                _v = new Vector2(x, y);
                if (_v == Vector2.right || _v == Vector2.left || _v == Vector2.up || _v == Vector2.down)
                {
                    RaycastHit2D hit = Physics2D.Raycast(roomAtual.GetComponent<Room>().scanRoomPos.position, _v, 55);
                    Vector3 v3 = new Vector3(_v.x, _v.y, 0);
                    Debug.DrawRay(roomAtual.GetComponent<Room>().scanRoomPos.position,_v,Color.red,10);

                    if (hit.collider)
                    {
                        count++;
                    }
                    else
                    {
                        v_saidaDesejada = _v;
                    }
                }
            } 
        }

        if (count >= 2)
        {
            if (v_saidaDesejada == Vector2.right)
            {
                saidaDesejada = "RIGHT";
            }
            if (v_saidaDesejada == Vector2.down)
            {
                saidaDesejada = "SOUTH";
            }
            if (v_saidaDesejada == Vector2.left)
            {
                saidaDesejada = "LEFT";
            }
            if (v_saidaDesejada == Vector2.up)
            {
                saidaDesejada = "NORTH";
            }

        }
        //nao preciso
        else
        {
            saidaDesejada = "NULL";

        }
        switch (entrance)
        {
            case "left":
                if (saidaDesejada != "NULL")
                {
                    for (int i = 0; i < leftEntranceRooms.Length; i++)
                    {
                        if (saidaDesejada == leftEntranceRooms[i].GetComponent<Room>().exit.ToString())
                        {
                            roomToinstanciate = leftEntranceRooms[i];
                        }
                        else
                        {

                            print("nao teve room com a saida desejada" + leftEntranceRooms[i].GetComponent<Room>().exit.ToString());
                        }
                    }
                }
                else
                {
                    roomToinstanciate = leftEntranceRooms[Shuffle.Range("leftEntrance", 0, leftEntranceRooms.Length)];
                }

                break;

            case "south":
                if (saidaDesejada != "NULL")
                {
                    for (int i = 0; i < southEntranceRooms.Length; i++)
                    {
                        if (southEntranceRooms[i].GetComponent<Room>().exit.ToString() == saidaDesejada)
                        {
                            roomToinstanciate = southEntranceRooms[i];
                        }
                    }
                }
                else
                {
                    roomToinstanciate = southEntranceRooms[Shuffle.Range("southEntrance", 0, southEntranceRooms.Length)];
                }
                break;

            case "right":
                if (saidaDesejada != "NULL")
                {

                    for (int i = 0; i < rightEntranceRooms.Length; i++)
                    {

                        if (rightEntranceRooms[i].GetComponent<Room>().exit.ToString() == saidaDesejada)
                        {
                            Debug.Log(saidaDesejada);
                            roomToinstanciate = rightEntranceRooms[i];
                        }
                    }
                }
                else
                {
                    roomToinstanciate = rightEntranceRooms[Shuffle.Range("rightEntrance", 0, rightEntranceRooms.Length)];
                }
                break;

            case "north":
                if (saidaDesejada != "NULL")
                {
                    for (int i = 0; i < northEntranceRooms.Length; i++)
                    {
                        if (northEntranceRooms[i].GetComponent<Room>().exit.ToString() == saidaDesejada)
                        {
                            roomToinstanciate = northEntranceRooms[i];
                        }
                    }
                }
                else
                {
                    roomToinstanciate = northEntranceRooms[Shuffle.Range("northEntrance", 0, northEntranceRooms.Length)];
                }
                break;
        }
        //roomAtual = roomToinstanciate;
        return roomToinstanciate;
    }


    public GameObject ChoseRoom()
    {
        int roomID = Random.Range(0, rooms.Length);
        return rooms[roomID];
    }

}
