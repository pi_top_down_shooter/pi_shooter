﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Race { HUMAN = 0, DWARF = 1, ELF = 2, ORC = 3, GNOME = 4, HALFLING }
public enum CharClass { ROGUE, PALADIN, MAGE, BLACKMISTH, ASSASSIN, WARRIOR, BARBARIAN }

public class RPG_Character : LivingBeing {

	public string charName = "Jubileu";
    public Race charRace = Race.ORC;
    public CharClass charClass = CharClass.BLACKMISTH;

    public float height = 1.7f;
    public float weight = 70f;

    [Range(1, 25)]
    public int str = 10;

    [Range(1, 25)]
    public int con = 10;

    [Range(1, 25)]
    public int dex = 10;

    [Range(1, 25)]
    public int wis = 10;

    [Range(1, 25)]
    public int cha = 10;

    [Range(1, 25)]
    public int itg = 10;

    public List<string> feats = new List<string>();

    void Start() {
        feats.Add("Dodge");
        feats.Add("Double Jump");
        feats.Add("Insta kill");
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Space))
            feats[0] = "Critical Hit";

        if (Input.GetKeyDown(KeyCode.A))
            feats.RemoveAt(1);
    }

    protected override void HitPointsCalculation() {
        switch (charClass) {
            case CharClass.ASSASSIN:
                maxHealth = 8;
                break;
            case CharClass.BARBARIAN:
                maxHealth = 12;
                break;
            case CharClass.MAGE:
                maxHealth = 4;
                break;
            case CharClass.ROGUE:
                maxHealth = 6;
                break;
            case CharClass.BLACKMISTH:
            case CharClass.WARRIOR:
            case CharClass.PALADIN:
                maxHealth = 10;
                break;
            default:
                maxHealth = 4;
                break;
        }


        base.HitPointsCalculation();
    }
}
