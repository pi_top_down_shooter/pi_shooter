﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Enemy : LivingBeing {

    public class ShortestPathStep
    {
        public Vector2 position;
        public int gScore = 0;
        public int hScore = 0;
        public ShortestPathStep parent = null;
        public int fScore = 0;

    }
    ShortestPathStep _shortestPathStep;
    List<int> fscores = new List<int>();
    int lowestF;
    List<ShortestPathStep> closedList = new List<ShortestPathStep>();
    List<ShortestPathStep> openList = new List<ShortestPathStep>();
    //List<ShortestPathStep> shortestPath = new List<ShortestPathStep>();
    public List<Vector2> tilesAdjacentes = new List<Vector2>();
    public List<Vector2> closedTiles = new List<Vector2>();
    public List<Vector2> path = new List<Vector2>();
    public List<Vector2> adj = new List<Vector2>();
    Vector2 newTIle;
    bool podeIr = true;

    GameObject playerPrefab;
    Player player;
    [SerializeField] GameObject goldObject;
    [SerializeField] GameObject[] droppableItems;
    Pooling pooling;
    Rigidbody2D rigid;
    [SerializeField]float moveRange;
    Vector2 direction;
    float dis;
    [SerializeField] SpriteRenderer enemySprite;
    bool canAttack;
    float attackCounter;
    [SerializeField] float attackRate = 0.5f;
    public Transform[] position;
    public List<Vector2> tiles = new List<Vector2>();
    public Transform bestTile;
    PathFinding _pathFinding;
    float posAtual;
    float posIdeal;
    public Vector2 tileAtual;
    bool firstScan = true;
    float pathTimer;
    private void Start()
    {
        _shortestPathStep = new ShortestPathStep();
        _shortestPathStep.position = new Vector2(Mathf.RoundToInt(transform.position.x) , Mathf.RoundToInt(transform.position.y));
        _shortestPathStep.gScore = 0;
        _shortestPathStep.hScore = 0;
        _shortestPathStep.parent = null;


        tileAtual = transform.position;
        pooling = GameObject.FindGameObjectWithTag("Pooling").GetComponent<Pooling>();
        playerPrefab = GameObject.FindGameObjectWithTag("Player");
        player = playerPrefab.GetComponent<Player>();
        _pathFinding = GetComponentInChildren<PathFinding>();
        exp = 10 * level;
        currentHealth = maxHealth;
        //GoToBestTile();
        
    }

    private void Update()
    {
        pathTimer += Time.deltaTime;
        if (pathTimer >0.5f)
        {
           // ResetPath();
            FindPath(player.transform.position);
            pathTimer = 0;
        }
        dis = Vector3.Distance(playerPrefab.transform.position, transform.position);
        if (dis <= moveRange)
        {

            
            if (Input.GetKeyDown(KeyCode.L))
            {
                
            }
            if (!CheckPath())
            {
               // FindPath(player.transform.position);
            }
            //Move2();

               // StartCoroutine(GoToBestTile());
            
               


            /*
            if (_pathFinding.tilesTotal.Count == _pathFinding.tilesToGo.Count)
            {
                direction = player.transform.position - transform.position;
                transform.Translate(direction * speed * Time.deltaTime);
                GoToBestTile();
            }
            else
            {
                transform.Translate(direction * speed * Time.deltaTime);
            }
            */
            
        }

        if (canAttack)
        {
            attackCounter += Time.deltaTime;
            if (attackCounter >= attackRate)
            {
                Attack();
            }
        }
    }

    public override void TakeDamage(float amount)
    {
        if (currentHealth > 1)
        {
            DisplayDamageText(amount);
            //anim e etc
        }
        base.TakeDamage(amount);
    }

    public override void Death()
    {
        player.UpdatePlayerExperience(exp);
        DropSystem();        
        base.Death();
        Destroy(gameObject);
    }

    public void DropSystem()
    {
        float chance = Random.Range(0f, 100f);
        float chanceToDropGold = 12.5f + (level * 0.25f);
        if (chance <= chanceToDropGold)
        {
            print("dropou gold");
            DropGold();
        }
        chance = Random.Range(0f, 100f);
        float chanceToDropItem = 7.5f + (level * 0.5f);
        
        if (chance <= chanceToDropItem)
        {
            print("dropou item");
            DropItem();
        }

    }


    private void DisplayDamageText(float amount)
    {
        GameObject newText = pooling.GetPooledDamageText();
        if (newText != null)
        {
            newText.transform.position = transform.position;
            newText.transform.rotation = transform.rotation;

            if (playerPrefab.GetComponent<Player>().hasCrit)
            {
                newText.GetComponent<TextMesh>().color = Color.yellow;
            }
            else
            {
                newText.GetComponent<TextMesh>().color = Color.gray;
            }
            newText.GetComponent<TextMesh>().text = amount.ToString();
            newText.SetActive(true);
        }
    }

    private void Move()
    {
        
        direction = playerPrefab.transform.position - transform.position;

        direction.Normalize();
        transform.Translate(direction * speed * Time.deltaTime);
        if (player.transform.position.x > transform.position.x)
        {
            enemySprite.flipX = false;
        }
        else
        {
            enemySprite.flipX = true;
        }


    }

    void Attack()
    {
        player.TakeDamage(damage);
        attackCounter = 0;
    }

    public IEnumerator GoToBestTile()
    {
        if (_pathFinding.tilesToGo.Count != _pathFinding.tilesTotal.Count)
        {

        }


        for (int i = 0; i < _pathFinding.tilesToGo.Count; i++)
        {
            if (bestTile == null)
            {
                bestTile = _pathFinding.tilesToGo[i].transform;
            }
            else
            {
                posAtual = Vector2.Distance(player.transform.position, transform.position);
                posIdeal = Vector2.Distance(player.transform.position, _pathFinding.tilesToGo[i].transform.position);

                if (posIdeal < posAtual)
                {
                    if (posIdeal < Vector2.Distance(playerPrefab.transform.position, bestTile.transform.position))
                    {
                        bestTile = _pathFinding.tilesToGo[i].transform;
                    }
                }
            }
        }
        yield return new WaitForSeconds(0.5f);
        if (bestTile != null &&_pathFinding.tilesToGo.Contains(bestTile.gameObject) )
        {
            transform.position = Vector2.MoveTowards(transform.position, bestTile.position, speed * Time.deltaTime);
        }
        else
        {
            bestTile = null;
        }
        
        




    }


    void SearchPath(Vector2 target)
    {
        
        if (firstScan)
        {
            tileAtual = transform.position;
        }
        _shortestPathStep.position = tileAtual;
        Debug.Log(tileAtual);
        if (tileAtual == target)
        {
            print("player");
            return;
        }
        //verifica se o ponto inicial ja esta na closed
        if (!closedTiles.Contains(tileAtual))
        {
            closedTiles.Add(tileAtual);
        }
        //procura adjacentes
        tilesAdjacentes.Clear();
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                Vector2 _v = new Vector2(x, y);
                if (_v == Vector2.right || _v == Vector2.up || _v == Vector2.left || _v == Vector2.down)
                {

                    Vector2 pos = _v + tileAtual;
                    RaycastHit2D ray = Physics2D.Raycast(pos, Vector2.down, 1);
                    if (!ray.collider || ray.collider.tag == "Player")
                    {
                        if (!closedTiles.Contains(tileAtual))
                        {
                            tilesAdjacentes.Add(pos);
                        }
                        
                    }
                }
            }
        }
        for (int i = 0; i < tilesAdjacentes.Count; i++)
        {
            ShortestPathStep step = new ShortestPathStep();
            step.position = tilesAdjacentes[i];
            step.parent = _shortestPathStep;
            step.gScore = step.parent.gScore + 1;
            step.hScore = ComputeHScoreFroomCoord(tilesAdjacentes[i], target);
            step.fScore = FScore(step);

                openList.Add(step);
                fscores.Add(step.fScore);
                Debug.Log("adicionou pra open list");
            
                for (int j = 0; j < openList.Count; j++)
                {
                    if (openList[i].position == step.position)
                    {
                        if (step.fScore < openList[i].fScore)
                        {
                            openList[i] = step;
                            openList[i].parent = _shortestPathStep;
                        }
                    }
                }
            

        }
        // Calcular menor FScore
        fscores.Clear();       
        if (fscores.Count != 0 && openList.Count != 0)
        {
            
            lowestF = fscores.Min();
        }
        else
        {
            Debug.Log("openList é 0");
        }
        ShortestPathStep newStep = new ShortestPathStep();
        for (int i = 0; i < openList.Count; i++)
        {
            if (openList[i].fScore <=lowestF)
            {
                newStep = openList[i];
                // adiciona o melhor caminho a lista path
                path.Add(newStep.position);
                // loop
                newStep.parent = _shortestPathStep;
                
            }
        }
        //adiciona pra closed
        
        _shortestPathStep = newStep;
        tileAtual = newStep.position;
        firstScan = false;
    }
























    void DoPath(Vector2 target)
    {
        target = new Vector2(Mathf.RoundToInt(target.x), Mathf.RoundToInt(target.y));
        for (int i = 0; i < path.Count; i++)
        {
            Vector2 enemyPos = new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
            Vector2 playerPos = new Vector2(Mathf.RoundToInt(player.transform.position.x), Mathf.RoundToInt(player.transform.position.y));
            if (enemyPos != path[i])
            {
                if (target != playerPos)
                {
                    print("eu sai");
                    return;
                    
                }
                transform.position = Vector2.MoveTowards(transform.position, path[i], speed * Time.deltaTime);
                
            }
            
        }
  
    }

    void FindPath(Vector2 target)
    {
        target = new Vector2(Mathf.RoundToInt(target.x), Mathf.RoundToInt(target.y));
        _shortestPathStep.position = new Vector2(Mathf.RoundToInt(transform.position.x) , Mathf.RoundToInt(transform.position.y));
        
        
        if (tileAtual == target)
        {
           
            return;
        }
        if (!closedTiles.Contains(tileAtual))
        {
            closedTiles.Add(_shortestPathStep.position);
            closedList.Add(_shortestPathStep);
            
            ShortestPathStep currentStep = new ShortestPathStep();
            currentStep.position = tileAtual;



        }
        openList.Clear();
        adj.Clear();
            tilesAdjacentes.Clear();
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    Vector2 _v = new Vector2(x, y);
                    if (_v == Vector2.right || _v == Vector2.up || _v == Vector2.down || _v == Vector2.left)
                    {
                        Vector2 pos = _v + tileAtual;
                         adj.Add(pos);
                        if (!closedTiles.Contains(pos))
                        {
                            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.down, 1);
                         if (!hit.collider || hit.collider.tag == "Player")
                         {
                            tilesAdjacentes.Add(pos);
                         }
                       
                        }
 
                    }

                }
            }
        if (tilesAdjacentes.Count == 0)
        {
            closedTiles.Clear();
            return;
        }
        for (int i = 0; i < tilesAdjacentes.Count; i++)
        {
            ShortestPathStep step = new ShortestPathStep();
            step.position = tilesAdjacentes[i];
            step.gScore = closedList[closedList.Count - 1].gScore + 1;
            step.hScore = ComputeHScoreFroomCoord(tilesAdjacentes[i], target);
            step.fScore = FScore(step);
            openList.Add(step);

        }

        fscores.Clear();
        
        for (int i = 0; i < openList.Count; i++)
        {
            fscores.Add(openList[i].fScore);
            //Debug.Log(openList[i].fScore + "f score = " + "g score " + openList[i].gScore + "h score " + openList[i].hScore);
        }

        if (fscores.Count != 0 && openList.Count != 0)
        {
            lowestF = fscores.Min();
        }
         
        
        ShortestPathStep newStep = new ShortestPathStep();
        for (int i = 0; i < openList.Count; i++)
        {
           
            if (openList[i].fScore <= lowestF)
            {
                newTIle = new Vector2(Mathf.RoundToInt(openList[i].position.x), Mathf.RoundToInt(openList[i].position.y));
                newStep = openList[i];
            }
        }
        if (!path.Contains(newTIle))
        {
            path.Add(newTIle);
            //transform.position = Vector2.MoveTowards(transform.position, newTIle, speed * Time.deltaTime);


        }
        
        if (!closedList.Contains(newStep))
        {
            closedList.Add(newStep);
        }
        if (!closedTiles.Contains(newTIle))
        {
            closedTiles.Add(newTIle);
        }
        
        
        tileAtual = newTIle;
        bestTile.position = path[path.Count - 1];
        //ResetPath();

    }

    void ResetPath()
    {
        path.Clear();
        closedList.Clear();
        closedTiles.Clear();
        tilesAdjacentes.Clear();
        adj.Clear();
        tileAtual = transform.position;
        openList.Clear();
    }

    int ComputeHScoreFroomCoord(Vector2 fromCoord, Vector2 toCoord)
    {
        int i = Mathf.RoundToInt(toCoord.x - fromCoord.x) + Mathf.RoundToInt(toCoord.y - fromCoord.y);
        return Mathf.Abs(i);
    }

    int FScore(ShortestPathStep step)
    {
        return step.gScore + step.hScore;
    }

    /*
     * posAtual = Vector2.Distance(target, tileAtual);
            posIdeal = Vector2.Distance(target, tilesAdjacentes[i]);
                if (posIdeal < posAtual)
                {
                    if (posIdeal<Vector2.Distance(target,bestTile.transform.position))
                    {
                    bestTile.position = tilesAdjacentes[i];
                    }
                }
                else
                {
                    
                }

     */

    bool CheckPath()
    {
        Vector2 target = new Vector2(Mathf.RoundToInt(player.transform.position.x), Mathf.RoundToInt(player.transform.position.y));
      
      

        if (tileAtual == target)
        {
            return true;
        }
        else
        {
            return false;
        }
    }





    private void OnDrawGizmos()
    {
        for (int i = 0; i < path.Count; i++)
        {
            Gizmos.DrawSphere(path[i], 0.2f);
            Gizmos.DrawLine(path[i], path[GetNextIndex(i)]);
        }
        Gizmos.DrawWireSphere(transform.position, moveRange);

    }

    int GetNextIndex(int i)
    {
        if (i+1 == path.Count)
        {
            return i;
        }
        else
        {
            return i + 1;
        }
    }



    void Move2()
    {
        for (int i = 0; i < _pathFinding.tilesToGo.Count; i++)
        {
            if (bestTile == null)
            {
                bestTile = _pathFinding.tilesToGo[i].transform;
            }
            else
            {
                posAtual = Vector2.Distance(player.transform.position,transform.position);
                posIdeal = Vector2.Distance(player.transform.position, _pathFinding.tilesToGo[i].transform.position);

                if (posIdeal< posAtual)
                {
                    if (posIdeal< Vector2.Distance(playerPrefab.transform.position, bestTile.transform.position))
                    {
                        bestTile = _pathFinding.tilesToGo[i].transform;
                    }
                }
}
        }

        if (_pathFinding.tilesToGo.Contains(bestTile.gameObject))
        {
            direction = bestTile.position - transform.position;
            transform.Translate(direction * speed * Time.deltaTime);
        }
        

       // _pathFinding.RestartTiles();
    }

    /*
    void Move1()
        
    {
        
        //detect tiles
        
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                Vector2 tile = new Vector2(x, y);
                tiles.Add(tile);
            }
        }
        
        //Select tiles
        for (int i = 0; i < position.Length; i++)
        {
            

            RaycastHit2D hit = Physics2D.Raycast(position[i].position, Vector2.down, 1.0f);
            if (!hit.collider)
            {
                tiles.Add(position[i].position);
            }
        }


        //Check distance
        for (int i = 0; i < tiles.Count; i++)
        {
            //Vector3 pos = new Vector3(transform.position.x + tiles[i].x, transform.position.y + tiles[i].y,0);
            
            float playerToTile = Vector2.Distance(playerPrefab.transform.position, tiles[i]);
            dis = Vector3.Distance(playerPrefab.transform.position, transform.position);

            
                if (playerToTile < dis)
                {
                    if (bestTile == null)
                    {
                        bestTile = tiles[i];
                    }
                    else
                    {                    
                        if (playerToTile < Vector2.Distance(playerPrefab.transform.position, bestTile))
                        {
                            bestTile = tiles[i];
                        }
                    }
                }
            
            

        }
        direction = bestTile - transform.position;
        transform.Translate(direction * speed * Time.deltaTime);
    }
   */

        
   /* void moveToward(Vector2 target)
    {
        Vector2 fromTileCoord = transform.position;
        Vector2 toTileCoord = target;
        if (fromTileCoord == toTileCoord)
        {
            print("chegou no destino");
            return;
        }
       
        if (CheckIfTileHasCollider(target))
        {
            return;
        }

        Debug.Log("from" + fromTileCoord + "to" + toTileCoord);

        bool pathFound = false;

        self.position = fromTileCoord;
        spOpenSteps.Add(self);
        do
        {
            RoomManager.ShortestPathStep currentStep = spOpenSteps[0];
            spClosedSteps.Add(currentStep);
            spOpenSteps.Remove(spOpenSteps[0]);
            if (currentStep.position == toTileCoord)
            {
                pathFound = true;
                RoomManager.ShortestPathStep tmpStep = currentStep;
                
                do
                {
                    Debug.Log("path found:" + tmpStep);
                    tmpStep = tmpStep.parent;
                } while (tmpStep != null);
                spOpenSteps = null;
                spClosedSteps = null;
                break;

            }
            List<RoomManager.ShortestPathStep> adjSteps = RoomManager.Instance.WalkableAdjacentTiles(currentStep.position);


            for (int i = 0; i < adjSteps.Count; i++)
            {
                RoomManager.ShortestPathStep step = adjSteps[i];
                if (spClosedSteps.Contains(step))
                {
                    continue;
                }
                int moveCost = CostToMoveFromStep(currentStep, step);

                if (!spOpenSteps.Contains(step))
                {
                    step.parent = currentStep;
                    step.gScore = currentStep.gScore + moveCost;
                    step.hScore = ComputeHScoreFroomCoord(step.position, toTileCoord);
                    //InsertInOpenSteps(step);

                }
                else
                {
                    int index = spOpenSteps.IndexOf(step);
                    step = spOpenSteps[index];

                    if ((currentStep.gScore + moveCost) < step.gScore)
                    {
                        step.gScore = currentStep.gScore + moveCost;
                        spOpenSteps.Remove(step);
                        //InsertInOpenSteps(step);

                    }
                }
            }
            /*
            foreach (RoomManager.ShortestPathStep v in adjSteps)
            {
                RoomManager.ShortestPathStep step = new RoomManager.ShortestPathStep();
                step = v;
                if (spClosedSteps.Contains(step))
                {
                    continue;
                }
                int moveCost = CostToMoveFromStep(currentStep, step);


                if (!spOpenSteps.Contains(step))
                {
                    step.parent = currentStep;
                    step.gScore = currentStep.gScore + moveCost;
                    step.hScore = ComputeHScoreFroomCoord(step.position, toTileCoord);
                    InsertInOpenSteps(step);

                }
                else
                {
                    int index = spOpenSteps.IndexOf(step);
                    step = spOpenSteps[index];

                    if ((currentStep.gScore + moveCost) < step.gScore)
                    {
                        step.gScore = currentStep.gScore + moveCost;
                        spOpenSteps.Remove(step);
                        InsertInOpenSteps(step);

                    }
                }
            }
            
        } while (spOpenSteps.Count > 0);

        if (!pathFound)
        {
            Debug.Log("bateu na parede");
        }
    }

    */







    







    bool CheckIfTileHasCollider(Vector2 target)
    {

        RaycastHit2D hit = Physics2D.Raycast(target, Vector2.down, 1f);
        if (hit.collider)
        {
            print("colidiu");
            return true;
        }
        else
        {
            return false;
        }
    }

    
    void AddTilesAdjacentes(Vector2 position)
    {
        //tilesAdjacentes.Clear();
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                Vector2 _v = new Vector2(x, y);
                if (_v == Vector2.right || _v == Vector2.left || _v == Vector2.up || _v == Vector2.down)
                {
                    Vector2 pos = new Vector2(position.x + _v.x, position.y + _v.y);

                   // tilesAdjacentes.Add(pos);
                }
            }
        }
       // InsertPathStepToTilesAdjacentes();
    }




    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Player>())
        {
            canAttack = true;
        }
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Player>())
        {
            canAttack = false;
        }
    }



    void DropGold()
    {
        float goldToDrop = Random.Range(1, 10f) * level * (1 + player.a_extraGold);
        goldToDrop = Mathf.Round(goldToDrop);
        GameObject goldItem = (GameObject)Instantiate(goldObject, transform.position, transform.rotation);
        goldItem.GetComponent<Gold>().gold = goldToDrop;

    }

    void DropItem()
    {
        int itemToDrop = Random.Range(0, droppableItems.Length);
        GameObject item;
        item = Instantiate(droppableItems[itemToDrop], transform.position, transform.rotation);
        item.GetComponent<Item>().level = Random.Range(Mathf.Min(level, level - 2), level + 2);
    }


}
