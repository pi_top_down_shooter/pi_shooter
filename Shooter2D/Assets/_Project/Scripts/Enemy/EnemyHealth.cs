﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : LivingBeing {
    
    bool isDead;
    GameObject playerPrefab;
    Pooling pooling;
    private void Start()
    {
        pooling = GameObject.FindGameObjectWithTag("Pooling").GetComponent<Pooling>();
        playerPrefab = GameObject.FindGameObjectWithTag("Player");
        currentHealth = maxHealth;
    }
    void Update()
    {

       
    }
    /*
    public void TakeDamage(float amout)
    {
        if (currentHealth > 1)
        {
            DisplayDamageText(amout);
            //anim e etc
        }

        currentHealth -= amout;

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }
    

    void Death()
    {
        isDead = true;
        // give exp to player
        playerPrefab.GetComponent<Player>().UpdatePlayerExperience(_enemy.exp);
        GetComponent<Enemy>().DropSystem();
        Destroy(gameObject);
    }
    */

    private void DisplayDamageText(float amount)
    {
        GameObject newText = pooling.GetPooledDamageText();
        if (newText != null)
        {
            newText.transform.position = transform.position;
            newText.transform.rotation = transform.rotation;

            if (playerPrefab.GetComponent<Player>().hasCrit)
            {
                newText.GetComponent<TextMesh>().color = Color.yellow;
            }
            else
            {
                newText.GetComponent<TextMesh>().color = Color.gray;
            }
            newText.GetComponent<TextMesh>().text = amount.ToString();
            newText.SetActive(true);
        }
    }
}
