﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFindingTile : MonoBehaviour {
    PathFinding _pathFinding;
    [SerializeField]Transform parent;
    public enum ColPosition { up,down,left,right}
    public ColPosition colPos;
    private void Start()
    {

        _pathFinding = GetComponentInParent<PathFinding>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (_pathFinding.layers.Contains(col.gameObject.layer) && _pathFinding.tilesToGo.Contains(gameObject))
        {
            _pathFinding.tilesToGo.Remove(gameObject);
        }
        //GetComponentInParent<Enemy>().StartCoroutine("GoToBestTile");
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (_pathFinding.layers.Contains(collision.gameObject.layer) && !_pathFinding.tilesToGo.Contains(gameObject))
        {
            _pathFinding.tilesToGo.Add(gameObject);
        }
    }


    void DetectColision(GameObject obj)
    {
        switch (colPos)
        {
            case ColPosition.up:

                break;
            case ColPosition.down:
                break;
            case ColPosition.left:
                break;
            case ColPosition.right:
                break;
            default:
                break;
        }


    }

    private void Update()
    {
        if (!_pathFinding.tilesToGo.Contains(gameObject))
        {

        }
    }


}
