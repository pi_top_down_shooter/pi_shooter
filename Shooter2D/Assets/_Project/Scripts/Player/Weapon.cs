﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [System.Flags]
    public enum WeaponType
    {
        Normal = 1,
        Random = 2
    }
    public WeaponType weaponType;

    public GameObject bullet;

    public float damage;

    public int maxAmmo;

    public int currentAmmo;

    public int maxAmmoInHand;

    public float reloadTime;

    public Pooling poolWeapon;

    public float fireRate;

    //public ParticleSystem muzzleEffect;

    public Transform muzzlePosition;

    public ParticleSystem bulletShell;

    public AudioClip shotSoundClip;

    [Header("Nao Mexer")]
    public List<GameObject> pooledList;
    public Transform[] bulletStartPos;
    public AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

}
