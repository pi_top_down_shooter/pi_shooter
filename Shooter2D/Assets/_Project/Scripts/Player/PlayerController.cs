﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    public Animator _animator;
    public GameObject weapon;
    public static bool IsLeft, IsRight, IsUp, IsDown;
    private float _LastX, _LastY;
    
    RaycastHit2D hit;
    Ray ray;
    public bool player1;
    Shooting _shooting;
    float moveX, moveY;
    float directionX;
    Vector2 direction;
    public GameObject _inventory,help;
    public string horizontalMovement, verticalMovement,shoot,inventory;
    public bool canShoot;
      public  bool gettingItem;

    public Transform turnObject;
    Manager _manager;
    [SerializeField] CanvasManager _canvasManager;
    public GameObject itemPosition;
    // Use this for initialization

   


    void Start () {
        _shooting = GetComponent<Shooting>();
    }
	
	// Update is called once per frame
	void Update ()
    {

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

        if (Time.timeScale != 0)
        {
            if (Input.GetMouseButton(0))
            {
                if (hit.collider)
                {
                    switch (hit.collider.gameObject.layer)
                    {
                        case 12:
                            gettingItem = true;
                            //hit.collider.gameObject.GetComponent<Item>()._OnMouseDown();
                            itemPosition = hit.collider.gameObject;
                            break;
                        default:
                            Shoot();
                            break;
                    }
                }
                else
                {
                    Shoot();
                }
            }

            Move();
            RotateMouse();

            if (Input.GetKeyDown(KeyCode.R) && !_shooting.isReloading)
            {
                _shooting.isReloading = true;
                _shooting.StartCoroutine("Reload");
                //anim.Play("Reload");

            }
        }





       
        DisplayInventory();

        if (_inventory.activeSelf )
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        // TEST
        if (Input.GetKeyDown(KeyCode.P))
        {
            SceneManager.LoadScene("cena");
        }      


        
    }

    private void RotateJoystick()
    {
        if (Input.GetAxis("Horizontal_Rotation") != 0 || Input.GetAxis("Vertical_Rotation") != 0)
        {
            float horizontal = Input.GetAxis("Horizontal_Rotation") * Time.deltaTime;
            float vertical = Input.GetAxis("Vertical_Rotation") * Time.deltaTime;
            float angle = Mathf.Atan2(vertical, horizontal) * Mathf.Rad2Deg;
            turnObject.transform.eulerAngles = new Vector3(0, 0, angle - 90);
        }
    }



    public void Move()
    {

        if (!gettingItem)
        {
            moveX = Input.GetAxisRaw(horizontalMovement);
            moveY = Input.GetAxisRaw(verticalMovement);
            Vector2 movement = new Vector2(moveX, moveY);
            movement.Normalize();
            transform.Translate(movement* GetComponent<Player>().speed * Time.deltaTime);
        }
        else
        {
            Vector2 target = itemPosition.transform.position - transform.position;
            target.Normalize();
            transform.Translate(target * (GetComponent<Player>().speed -1 ) * Time.deltaTime);
        }

        if (player1)
        {

            
            if (Input.GetAxis(horizontalMovement) == 0)
            {
                if (Input.GetAxis(verticalMovement) > 0)
                {
                    _animator.SetBool("run", false);
                    _animator.SetBool("runback", true);
                }
                if (Input.GetAxis(verticalMovement) < 0)
                {
                    _animator.SetBool("run", true);
                    _animator.SetBool("runback", false);
                }
                if (Input.GetAxis(verticalMovement) == 0)
                {
                    _animator.SetBool("run", false);
                    _animator.SetBool("runback", false);
                }
            }

            if (Input.GetAxis(horizontalMovement) != 0)
            {
                if (Input.GetAxis(verticalMovement) <= 0)
                {
                    gettingItem = false;
                    _animator.SetBool("run", true);
                    _animator.SetBool("runback", false);
                }
                else if (Input.GetAxis(verticalMovement) > 0)
                {
                    gettingItem = false;
                    _animator.SetBool("runback", true);
                    _animator.SetBool("run", false);
                }


            }

        }
        




    }

   

    void RotateMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        directionX = (mousePosition.x - transform.position.x);
        direction = new Vector2(directionX, mousePosition.y - transform.position.y);

        turnObject.up = direction;

        if (player1)
        {
            if (mousePosition.x < transform.position.x)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                transform.localScale = new Vector3(1, 1, 1);
            }

            if (mousePosition.y > transform.position.y)
            {
                weapon = GameObject.FindGameObjectWithTag("Weapon");
                weapon.GetComponentInChildren<SpriteRenderer>().sortingOrder = 0;

            }
            else
            {
                weapon = GameObject.FindGameObjectWithTag("Weapon");
                weapon.GetComponentInChildren<SpriteRenderer>().sortingOrder = 100;
            }
        }
        

    }

    void Shoot()
    {

         _shooting.Shoot();
        gettingItem = false;



    }

    void DisplayInventory()
    {

            if (Input.GetKeyDown(KeyCode.I))
            {
                _inventory.SetActive(!_inventory.activeSelf);

            }
 

    }

    private void DPadInputSystem()
    {
        float x = Input.GetAxis("DPad X");
        float y = Input.GetAxis("DPad Y");

        IsLeft = false;
        IsRight = false;
        IsUp = false;
        IsDown = false;

        if (_LastX != x)
        {
            if (x == -1)
                IsLeft = true;
            else if (x == 1)
                IsRight = true;
        }

        if (_LastY != y)
        {
            if (y == -1)
                IsDown = true;
            else if (y == 1)
                IsUp = true;
        }

        _LastX = x;
        _LastY = y;

        if (IsUp)
        {
            _inventory.SetActive(!_inventory.activeSelf);
        }
    }
}

