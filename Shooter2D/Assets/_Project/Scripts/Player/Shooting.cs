﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {
    Weapon _weapon;
    float timer;
    [SerializeField] Transform body,weapon;
    Pooling pooling;
    [SerializeField] GameObject _canvas;
    public bool isReloading;
	// Use this for initialization
	void Start () {
        pooling = GameObject.FindGameObjectWithTag("Pooling").GetComponent<Pooling>();   
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

       
    }

    public void Shoot()
    {
        if (_canvas.activeSelf)
        {
            return;
        }
        foreach (GameObject armas in weapon.GetComponent<ChangeWeapon>().armas)
        {
            if (armas.activeSelf)
            {
                _weapon = armas.GetComponent<Weapon>();
            }
        }
       // _weapon = weapon.GetComponent<ChangeWeapon>().ar;
        string weaponType = _weapon.weaponType.ToString();
        switch (weaponType)
        {
            case "Normal":
                if (timer >= _weapon.fireRate && _weapon.currentAmmo > 0)
                {
                    for (int y = 0; y < _weapon.bulletStartPos.Length; y++)
                    {
                        GameObject newBullet = _weapon.poolWeapon.GetPooledObject(_weapon.pooledList);
                        GameObject newMuzzle = pooling.GetPooledMuzzle();

                        if (newBullet != null)
                        {
                            _weapon.bulletShell.Play();
                            newBullet.transform.position = _weapon.bulletStartPos[y].position;
                            newBullet.transform.rotation = _weapon.bulletStartPos[y].rotation;
                            newBullet.SetActive(true);


                        }
                        if (newMuzzle != null)
                        {
                            newMuzzle.transform.position = _weapon.muzzlePosition.position;
                            newMuzzle.transform.rotation = _weapon.muzzlePosition.rotation;
                            newMuzzle.SetActive(true);
                        }

                        _weapon.currentAmmo--;
                    }
                    //GetComponent<Animator>().SetTrigger("Recoil");
                    _weapon._audioSource.Stop();
                    _weapon._audioSource.PlayOneShot(_weapon.shotSoundClip, .15f);
                    timer = 0;
                    CanvasManager.Instance.UpdateAmmoCanvas();

                }
                else if (_weapon.currentAmmo <= 0)
                {

                    //gunAudio.PlayOneShot(emptyClip, 0.5f);
                    timer = 0;
                }
                break;
            case "Random":
                if (timer >= _weapon.fireRate && _weapon.currentAmmo > 0)
                {
                    //gunAudio.PlayOneShot(weapon.weaponClip, 0.25f);
                    int spawnPointIndex = Random.Range(0, _weapon.bulletStartPos.Length);
                    GameObject newBullet = _weapon.poolWeapon.GetPooledObject(_weapon.pooledList);
                    if (newBullet != null)
                    {
                        newBullet.transform.position = _weapon.bulletStartPos[spawnPointIndex].position;
                        newBullet.transform.rotation = _weapon.bulletStartPos[spawnPointIndex].rotation;
                        newBullet.SetActive(true);

                    }
                    _weapon.currentAmmo--;
                    timer = 0;
                    _weapon._audioSource.Stop();
                    _weapon._audioSource.PlayOneShot(_weapon.shotSoundClip, .15f);
                    CanvasManager.Instance.UpdateAmmoCanvas();
                }
                else if (_weapon.currentAmmo <= 0)
                {

                    //gunAudio.PlayOneShot(emptyClip, 0.5f);
                    timer = 0;
                }
                break;


            default:
                return;
        }
    }


    

    public IEnumerator Reload()
    {

       // _weapon.gunAudio.PlayOneShot(reloadClip, 0.3f);

        yield return new WaitForSeconds(_weapon.reloadTime);

        int ammoLimit;
        ammoLimit = _weapon.maxAmmoInHand - _weapon.currentAmmo;

        int condicao;
        condicao = _weapon.maxAmmo - ammoLimit;

        if (condicao < 0)
        {
            _weapon.currentAmmo += _weapon.maxAmmo;
            _weapon.maxAmmo = 0;
            isReloading = false;
        }
        else
        {
            _weapon.currentAmmo += ammoLimit;
            _weapon.maxAmmo -= ammoLimit;
            isReloading = false;


        }

        CanvasManager.Instance.UpdateAmmoCanvas();

    }

}
