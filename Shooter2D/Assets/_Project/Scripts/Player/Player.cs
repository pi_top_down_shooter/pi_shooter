﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : LivingBeing {
    static Player instance;
    public static Player Instance { get { return instance; } }

    public float currentGold;
    float damageMultiplier;
    GameObject weapon;
    CharacterSlot charSlot;
    [HideInInspector]
    public float a_critDamage, a_critChance, a_lifeSteal, a_regen, a_armor, a_extraExp, a_extraGold, a_bulletCapacity, a_spellDamage;
    bool damaged;
    [Header("Experiencia")]
    public float currentExp;
    public float expToLevelUp;
    [Header("Atributos")]
    public bool hasCrit;
    public float currentDamage;
    [SerializeField] GameObject[] slots;
    [SerializeField] GameObject playerStats;
    [SerializeField] Image hpBar,expBar;
    [SerializeField] CanvasManager _canvasManager;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        currentHealth = maxHealth;
        level = 1;
        expToLevelUp = 100;
        UpdatePlayerStats();        
    }

    private void Update()
    {
        if (isAlive)
        {


            UpdateHPBar();

            if (currentHealth < maxHealth)
            {
                currentHealth += (0.5f + a_regen) * Time.deltaTime;

            }

            if (damaged)
            {
                // damageImage
            }
            else
            {
                //damageImage
            }
            damaged = false;
        }
  
    }

    public override void TakeDamage(float amout)
    {

        if (currentHealth > 0)
        {
            //anim e etc
        }

        damaged = true;
        damageMultiplier = 1 - (0.05f * a_armor / (1 + 0.05f * Mathf.Abs(a_armor)));
        currentHealth -= amout * damageMultiplier;

        if (currentHealth <= 0 && isAlive)
        {
            Death();
        }

    }

    public override void Death()
    {
        GetComponent<PlayerController>()._inventory.SetActive(!GetComponent<PlayerController>()._inventory.activeSelf);
        base.Death();
    }

    public void UpdatePlayerStats()
    {
        ResetValues();
        //------------ Update stats with item's SECUNDARY atributes
        for (int i = 0; i < slots.Length; i++)
        {
            charSlot = slots[i].GetComponent<CharacterSlot>();

            if (charSlot.selectedItem != null)
            {
                damage += charSlot.selectedItem.GetComponent<Item>().a_damage + charSlot.selectedItem.GetComponent<Item>().damageDefault;
                a_critDamage += charSlot.selectedItem.GetComponent<Item>().a_critDamage + charSlot.selectedItem.GetComponent<Item>().critDmgDefault;
                a_critChance += charSlot.selectedItem.GetComponent<Item>().a_critChance + charSlot.selectedItem.GetComponent<Item>().critChanceDefault;
                a_lifeSteal += charSlot.selectedItem.GetComponent<Item>().a_lifeSteal + charSlot.selectedItem.GetComponent<Item>().lifeStealDefault;
                speed += charSlot.selectedItem.GetComponent<Item>().a_movementSpeed + charSlot.selectedItem.GetComponent<Item>().msDefault;
                a_regen += charSlot.selectedItem.GetComponent<Item>().a_regen + charSlot.selectedItem.GetComponent<Item>().regenDefault;
                a_armor += charSlot.selectedItem.GetComponent<Item>().a_armor + charSlot.selectedItem.GetComponent<Item>().armorDefault;
                a_extraExp += charSlot.selectedItem.GetComponent<Item>().a_extraExp + charSlot.selectedItem.GetComponent<Item>().expDefault;
                a_extraGold += charSlot.selectedItem.GetComponent<Item>().a_extraGold + charSlot.selectedItem.GetComponent<Item>().goldDefault;
                a_bulletCapacity += charSlot.selectedItem.GetComponent<Item>().a_bulletCapacity;
                a_spellDamage += charSlot.selectedItem.GetComponent<Item>().a_spellDamage;

            }

        }
        //------------ Update stats with item's PRIMARY atributes
        currentDamage = damage;
    }

    public void UpdatePlayerExperience(float amount)
    {
        currentExp += amount * (1 + a_extraExp);
        if (currentExp >= expToLevelUp)
        {
            level++;
            currentExp -= expToLevelUp;
            _canvasManager.LevelText();
            if (level <= 10)
            {
                expToLevelUp = level * 100;
            }
            else
            {
                expToLevelUp = level * 105;
            }

        }

        UpdateExpBar();
    }

    void ResetValues()
    {
        damage = 0;
        a_critDamage = 0.25f;
        a_critChance = 0.05f;
        a_lifeSteal = 0;
        speed = startSpeed;
        a_regen = 0;
        a_armor = 0;
        a_extraExp = 0;
        a_extraGold = 0;
        a_bulletCapacity = 0;
        a_spellDamage = 0;
    }

    public float RollPlayerDamage()
    {
        float _damage;
        if (a_critChance > 0)
        {
            float rollCritChance = Random.Range(0f, 1f);
            if (rollCritChance <= a_critChance)
            {
                print("critou");
                _damage = (damage) * (1 + a_critDamage);
                hasCrit = true;
                Healing(_damage * a_lifeSteal);
                return RoundNumber(_damage);

            }
        }
        _damage = damage;
        hasCrit = false;

        Healing(_damage * a_lifeSteal);
        return RoundNumber(_damage);
    }
    /*
    public void SimulateDamage(ItemStatsUI _item,ItemStatsUI _itemEquipado)
    {
        float _damage;
        float _damageSum = 0;
        float possibleDamage;
        float possibleDamageSum = 0;
        float defaultDmg = damage - _itemEquipado.selectedItem.GetComponent<Item>().damageDefault + _item.selectedItem.GetComponent<Item>().damageDefault - _itemEquipado.selectedItem.GetComponent<Item>().a_damage + _item.selectedItem.GetComponent<Item>().a_damage;
        float critChance = a_critChance - _itemEquipado.selectedItem.GetComponent<Item>().critChanceDefault + _item.selectedItem.GetComponent<Item>().critChanceDefault - _itemEquipado.selectedItem.GetComponent<Item>().a_critChance + _item.selectedItem.GetComponent<Item>().a_critChance;
        float critDmg = a_critDamage - _itemEquipado.selectedItem.GetComponent<Item>().critDmgDefault + _item.selectedItem.GetComponent<Item>().critDmgDefault - _itemEquipado.selectedItem.GetComponent<Item>().a_critDamage + _item.selectedItem.GetComponent<Item>().a_critDamage;
        for (int i = 0; i < 29; i++)
        {
            if (a_critChance > 0)
            {
                float rollCritChance = Random.Range(0f, 1f);
                if (rollCritChance <= a_critChance)
                {

                    _damage = (damage) * (1 + a_critDamage);
                    _damageSum += _damage;

                }
                else
                {
                    _damage = damage;
                    _damageSum += _damage;
                }
            }


        }


        for (int i = 0; i < 29; i++)
        {
            if (critChance > 0)
            {
                float rollCritChance = Random.Range(0f, 1f);
                if (rollCritChance <= critChance)
                {

                    possibleDamage = (defaultDmg) * (1 + critDmg);
                    possibleDamageSum += possibleDamage;

                }
                else
                {
                    possibleDamage = defaultDmg;
                    possibleDamageSum += possibleDamage;
                }
            }
        }
        if (possibleDamageSum > _damageSum)
        {
            string result = "dano +" + ((possibleDamageSum / _damageSum - 1) * 100).ToString("F1") + "%";
        }
        else
        {
            string result = "dano: -" + ((_damageSum / possibleDamageSum - 1) * 100).ToString("F1") + "%";
        }
        
        


    }
    */
    float RoundNumber(float number)
    {
        number *= 100;
        number = Mathf.Round(number);
        number /= 100;
        return number;
    }

    void UpdateHPBar()
    {
        float barValue = (float)currentHealth / maxHealth;
        hpBar.fillAmount = barValue;
    }

    void UpdateExpBar()
    {
        float expValue = (float)currentExp / expToLevelUp;
        expBar.fillAmount = expValue;
    }

}
